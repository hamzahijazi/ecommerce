<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" 

	%>
	<%@ page import=" java.io.IOException,java.io.PrintWriter,java.util.Optional,UserAuth.UserDao,UserAuth.User" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!-- Bootstrap CSS -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
	integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk"
	crossorigin="anonymous">

<title>Email Verification</title>
</head>
<body>

	<div class="card" style="width: 50%; margin-top: 25%; margin-left: 25%">
		<form method="POST" action="http://localhost:8080/ECommerceTask/verify.jsp">
			<div class="col-md-4 mb-3">
				<label for="validationServerUsername">Verification Code</label>
				<div class="input-group">
					<div class="input-group-prepend">
						<span class="input-group-text" id="inputGroupPrepend3">H-</span>
					</div>
					
					<input type="text"
					<%
					
						String userIDKey = "userID";
						String userID ;
						String userVerifykey = "userVerify";
						String userVerify = "1";
						
						userID=(String)session.getAttribute(userIDKey);
						UserDao userDao;
						userDao = UserDao.getInstance();
						String userOTP=userDao.getUserOTP(userID).get();
						
						
						
						
						PrintWriter output = response.getWriter();
						if((String)request.getParameter("OTP")!=null){
						if( ((String)request.getParameter("OTP")).equals(userOTP)){
						out.print( "class=\"form-control is-valid\" ");
						userDao.SetVerified(userID);
						session.setAttribute(userVerifykey,userVerify);
						session.setAttribute(userIDKey,userID);
						response.sendRedirect("/ECommerceTask/USER/Main.jsp");
						}
						out.print( "class=\"form-control is-invalid\" ");
						}
						System.out.print((String)request.getParameter("OTP"));
						
					%>
						id="OTP" name="OTP"
						aria-describedby="inputGroupPrepend3" required>
					<div class="invalid-feedback">Please Verification Code right.
					</div>
				</div>
				<button class="btn btn-primary" type="submit">Submit form</button>
			</div>
		</form>
	</div>







	<!-- Optional JavaScript -->
	<!-- jQuery first, then Popper.js, then Bootstrap JS -->
	<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
		integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
		crossorigin="anonymous"></script>
	<script
		src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
		integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
		crossorigin="anonymous"></script>
	<script
		src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"
		integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI"
		crossorigin="anonymous"></script>

</body>
</html>