<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page
	import=" java.io.IOException,java.io.PrintWriter,java.util.Optional,UserAuth.UserDao,UserAuth.User,java.util.Calendar,java.util.GregorianCalendar"%>
<body>
 <nav class="navbar navbar-dark bg-dark">
		       <a class="navbar-brand" href="http://localhost:8080/ECommerceTask/USER/Main.jsp">Hamza ECommerace</a>
		       <% response.setIntHeader("Refresh",1);
			      // Get current time
			      Calendar calendar = new GregorianCalendar();
			      String am_pm;
			      int hour = calendar.get(Calendar.HOUR);
			      int minute = calendar.get(Calendar.MINUTE);
			      int second = calendar.get(Calendar.SECOND);
			      
			      if(calendar.get(Calendar.AM_PM) == 0)
			        am_pm = "AM";
			      else
			        am_pm = "PM";
			 
			      String CT = hour+":"+ minute +" "+ am_pm;
		     out.println( " <a class=\"navbar-brand\" href=\"#\">"+ CT+"</a>");
		       %>
		       <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
		         <span class="navbar-toggler-icon"></span>
		       </button>
		       <div class="collapse navbar-collapse" id="navbarText">
		         <ul class="navbar-nav mr-auto">
		           <li class="nav-item active">
		             <a class="nav-link" href="http://localhost:8080/ECommerceTask/USER/Cart">Cart <span class="sr-only">(current)</span></a>
		           </li>
		           <li class="nav-item">
		             <a class="nav-link" name="logout"  value="1" href="http://localhost:8080/ECommerceTask/USER/LogOut">LogOut</a>
		           </li>
		       
		         </ul>
		      
		       </div>
		     
		     </nav>
</body>
