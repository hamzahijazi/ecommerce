<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page
	import=" java.io.IOException,java.io.PrintWriter,java.util.Optional,UserAuth.UserDao,UserAuth.User,Products.*,java.util.List"%>
<jsp:include page="Navbar.jsp" />
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!-- Bootstrap CSS -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
	integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk"
	crossorigin="anonymous">

<title>Home page</title>

</head>
<style>
.rcorners2 {
	border-radius: 15px 50px 30px;
	padding: 20px;
	background: #00C0C0;
	text-color: #00C0C0;
}


.rcorners1 {
	border-radius: 15px 50px 30px;
	padding: 20px;
	background: #C0C0C0;
	text-color: #00C0C0;
}
</style>
<body>
	<div class="card-body" style="margin-left: 10%;">

		<%
			ProductFactory productsObject = new ProductFactory();

		Product productRepository = productsObject.getProduct("File");
		List<ProductData> products = productRepository.getProduct();

		for (ProductData product : products) {
		%>
		<div class="rcorners2"
			style="display: inline-block; margin-left: 5rem; margin-top: 6rem;">
			<div class="card"
				style="width: 20rem; border-radius: 15px 50px 30px;">
				<div class="card-body">
				<img class="card-img-top" style="width:95%; border-radius: 15px 50px 30px;" src="https://www.cycleworld.com/resizer/s4jDG0jBLAFlfU9QcZbdXxYiaNI=/799x599/cloudfront-us-east-1.images.arcpublishing.com/bonnier/34E5ITNNQJEHNGS7PUQ3TEAU6Y.jpg" alt="Card image">
					<h5 class="card-title">
						<%
							out.println(product.getProductName());
						%>
					</h5>
				</div>
				<div class="card-body">


					<form action="/ECommerceTask/USER/DetailsProduct" method="post">

						<input type="submit" class="btn btn-secondary" value="Details"
							style="width: 50%; margin-left: 25%;">
							<input type="hidden" name="productcode" value="<%out.println(product.getProductCode());    %>">
					</form>
						<form action="/ECommerceTask/USER/AddToCart" method="post">

						 <input
							type="submit" class="btn btn-secondary rcorners2"
							value="Add Cart" style="width: 50%; margin-left: 25%;">
							<input type="hidden" name="productcode" value="<%out.println(product.getProductCode());    %>">
					</form>

				</div>
			</div>
		</div>
		<%
			}
		%>
	</div>
	<!-- Optional JavaScript -->
	<!-- jQuery first, then Popper.js, then Bootstrap JS -->
	<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
		integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
		crossorigin="anonymous"></script>
	<script
		src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
		integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
		crossorigin="anonymous"></script>
	<script
		src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"
		integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI"
		crossorigin="anonymous"></script>
</body>
</html>