<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page
	import=" java.io.IOException,java.io.PrintWriter,java.util.Optional,UserAuth.UserDao,UserAuth.User,Products.*,java.util.List"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!-- Bootstrap CSS -->
<link
	href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css"
	rel="stylesheet" id="bootstrap-css">
<script
	src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>

<title>Forget Password</title>

</head>
<style>
.rcorners2 {
	border-radius: 15px 50px 30px;
	padding: 20px;
	background: #00C0C0;
	text-color: #00C0C0;
}


.rcorners1 {
	border-radius: 15px 50px 30px;
	padding: 20px;
	background: #C0C0C0;
	text-color: #00C0C0;
}
</style>
<body>
	<div class="form-gap "></div>
	<div class="container ">
		<div class="row">
			<div class="col-md-4 col-md-offset-4 rcorners2">
				<div class="panel panel-default rcorners1 ">
					<div style="background: #ffffff; border-radius: 15px 50px 30px;">
						<div class="panel-body ">
							<div class="text-center ">
								<h3>
									<i class="fa fa-lock fa-4x"></i>
								</h3>
								<h2 class="text-center">Forgot Password?</h2>
								<p>You can reset your password here.</p>
								<div class="panel-body">

									<form class="form" method="post" action="/ECommerceTask/ForgetPassword">

										<div class="form-group">
											<div class="input-group">
												<span class="input-group-addon"><i
													class="glyphicon glyphicon-envelope color-blue"></i></span> <input
													id="email" name="email" placeholder="email address"
													class="form-control" type="email">
											</div>
										</div>
										<div class="form-group">
											<input name="recover-submit"
												class="btn btn-lg btn-primary btn-block"
												 type="submit">
										</div>

										<input type="hidden" class="hide" name="token" id="token"
											value="">
									</form>

								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>