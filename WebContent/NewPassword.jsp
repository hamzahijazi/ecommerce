<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page
	import=" java.io.IOException,java.io.PrintWriter,java.util.Optional,UserAuth.UserDao,UserAuth.User,Products.*,java.util.List"%>

<!DOCTYPE html>
<html>
<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<!-- Bootstrap CSS -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
	integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk"
	crossorigin="anonymous">

<title>New Password</title>

<style>
.rcorners2 {
	border-radius: 15px 50px 30px;
	padding: 20px;
	background: #00C0C0;
	text-color: #00C0C0;
}

.rcorners1 {
	border-radius: 15px 50px 30px;
	padding: 20px;
	background: #C0C0C0;
	text-color: #00C0C0;
}
</style>

</head>

<body>
<%

UserDao userDao;
userDao = UserDao.getInstance();
System.out.println(request.getParameter("time")+"hellothere");
String userid=request.getParameter("usercode");
long time = Long.valueOf(request.getParameter("time"));
if((System.currentTimeMillis()/1000)-time >30)
{
	User user=userDao.getUser(userid).get();
	RequestDispatcher reqdis= request.getRequestDispatcher("/ForgetPassword");
	request.setAttribute("valid", "notValid");
	request.setAttribute("email", user.getEmail());
	reqdis.include(request, response);
	out.print("not valid rquest check your email again");
}
else{


%>

	<div class="col-md-4 col-md-offset-4 rcorners2">
		<div class="panel panel-default rcorners1 ">
			<div style="background: #ffffff; padding:15px; border-radius: 15px 50px 30px;">
				<form method="post" action="/ECommerceTask/ForgetPassword">

			

					<div class="form-group">

						<label for="exampleInputPassword">Password</label> <input
							type="Password" name="newPassword" class="form-control" value=""
							required id="exampleInputPassword"> <small id="emailHelp"
							class="form-text text-muted">We'll never share your
							password with anyone else.</small>
					</div>
					<div class="form-group">
						<label for="exampleInputPassword1">Re-Password</label> <input
							type="Password" name="newPassword2" class="form-control"
							id="exampleInputPassword1" value="" required>
					</div>
					<input type="hidden" name="userid" value=<%out.print(userid); %>>
					<input type="hidden" name="valid" value="valid">
					<button type="submit" class="btn btn-primary">Continue</button>
				</form>
			</div>
		</div>
	</div>
<%} %>
	<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
		integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
		crossorigin="anonymous"></script>
	<script
		src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
		integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
		crossorigin="anonymous"></script>
	<script
		src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"
		integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI"
		crossorigin="anonymous"></script>
</body>
</html>