package UserAuth;

import java.util.List;
import java.util.Optional;

import org.jdbi.v3.core.Jdbi;

public class UserDao {

	static UserDao Instance;
	String connectionUrl = "jdbc:mysql://localhost:3306/servlet?useSSL=false";
	String password = "root";
	String Username = "root";
	Jdbi jdbi;

	private UserDao() throws ClassNotFoundException {
		Class.forName("com.mysql.jdbc.Driver");

		jdbi = Jdbi.create(connectionUrl, Username, password);

	}

	static public UserDao getInstance() throws ClassNotFoundException {
		if (Instance == null) {
			Instance = new UserDao();
			return Instance;
		} else {
			return Instance;
		}

	}




	public Optional<User> getUser(String id) {
		Optional<User> user = jdbi.withHandle(handle -> {

			return handle.createQuery("SELECT * FROM user where id=:id").bind("id", id).mapToBean(User.class)
					.findFirst();

		});

		return user;

	}

	public List<User> getAllUser() {
		List<User> user = jdbi.withHandle(handle -> {

			return handle.createQuery("SELECT * FROM user").mapToBean(User.class).list();

		});

		return user;

	}

	public User signUp(User user) {
		jdbi.useHandle(handle -> {

			handle.createUpdate(
					"INSERT INTO user(firstname,lastname,username,email,password,verifycode) VALUES (:firstname,:lastname,:username,:email,:password,:verifycode)")
					.bindBean(user).execute();

		});
		
		Optional<User> user1=	jdbi.withHandle(handle -> {

			return handle.createQuery(
					"SELECT * FROM user where email=:email")
			.bind("email", user.email).mapToBean(User.class).findFirst();
					

		});
		return user1.get();
	}

	public Optional<User> signIn(String email, String password) {
		Optional<User> user = jdbi.withHandle(handle -> {

			return handle.createQuery("SELECT * FROM user where email=:email && password=:password")
					.bind("email", email).bind("password", password).mapToBean(User.class).findFirst();

		});

		return user;

	}
	public Optional<String> getUserOTP(String id) {
		Optional<String> code = jdbi.withHandle(handle -> {

			return handle.createQuery("SELECT  verifycode from user where id=:id").bind("id", id).mapTo(String.class).findOne();
					

		});

		return code;

	}
	public void SetVerified(String id) {
		 jdbi.useHandle(handle -> {

			 handle.createUpdate("Update  user set verfied = 1 where id=:id").bind("id", id).execute();
					

		});

	

	}
	public void reSetPassword(String id,String password) {
		 jdbi.useHandle(handle -> {

			 handle.createUpdate("Update  user set password = :password where id=:id")
			 .bind("id", id)
			 .bind("password", password)
			 .execute();
					

		});

	

	}
	public User getUserByEmail(String email) {
		Optional<User> user1=	 jdbi.withHandle(handle -> {

			return handle.createQuery(
					"SELECT * FROM user where email=:email")
			.bind("email", email).mapToBean(User.class).findFirst();
					

		});
		return user1.get();

	

	}

}
