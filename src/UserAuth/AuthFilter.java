package UserAuth;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet Filter implementation class AuthFilter
 */
@WebFilter("/*")
public class AuthFilter implements Filter {

	/**
	 * Default constructor.
	 */
	private ServletContext context;

	public AuthFilter() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		// TODO Auto-generated method stub
		// place your code here

		// pass the request along the filter chain

		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse res = (HttpServletResponse) response;
		String userIDKey = "userID";
		String userVerifykey = "userVerify";
		String start = "/USER";
		HttpSession session = req.getSession(true);
		if (req.getServletPath().startsWith(start)) {

			if (session.getAttribute(userIDKey) == null || session.getAttribute(userVerifykey) == null) {
				res.sendRedirect("/ECommerceTask/SIgnIn");
				return;
			}
			chain.doFilter(request, response);

		} else {
			if (session.getAttribute(userIDKey) != null && session.getAttribute(userVerifykey) != null) {
				res.sendRedirect("/ECommerceTask/USER/Main.jsp");
				return;
			}
			chain.doFilter(request, response);
		}
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		this.context = fConfig.getServletContext();
		this.context.log("AuthenticationFilter initialized");
	}

}
