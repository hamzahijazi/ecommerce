package UserAuth;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Optional;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import Products.ProductFactory;

/**
 * Servlet implementation class SIgnIn
 */
@WebServlet("/SIgnIn")
public class SIgnIn extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public SIgnIn() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession(true);

		response.setContentType("text/html");
		String email;
		String password;
		String userIDKey = "userID";
		String userID = "ABCD";
		String userVerifykey = "userVerify";
		String userVerify = "0";
		// Allocate a output writer to write the response message into the network
		// socket
		PrintWriter out = response.getWriter();
		// Get session creation time.


		out.print("<!doctype html>");
		out.print("<html lang=\"en\">");
		out.print("");
		out.print("  <head>");
		out.print("    <!-- Required meta tags -->");
		out.print("    <meta charset=\"utf-8\">");
		out.print("    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\">");
		out.print("");
		out.print("    <!-- Bootstrap CSS -->");
		out.print(
				"    <link rel=\"stylesheet\" href=\"https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css\" integrity=\"sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk\" crossorigin=\"anonymous\">");
		out.print("");
		out.print("    <title>Hello, world!</title>");
		out.print("    ");
		out.print("  ");
		out.print("    </head>");
		out.print("    <style>");

		out.print(".rcorners2 {\r\n" + 
				"	border-radius: 15px 50px 30px;\r\n" + 
				"	padding: 20px;\r\n" + 
				"	background: #00C0C0;\r\n" + 
				"	text-color: #00C0C0;\r\n" + 
				"}"
				+ ".rcorners1 {\r\n" + 
				"	border-radius: 15px 50px 30px;\r\n" + 
				"	padding: 20px;\r\n" + 
				"	background: #ffffff;\r\n" + 
				"	text-color: #00C0C0;\r\n" + 
				"} ");
		
		out.print("   .error {");
		out.print("  background:#e8505b ;");
		out.print("   padding: 10px ;");
		out.print("   border-radius:15px 50px 30px; ;");
		out.print("  position: relative; ");
		out.print("   display: inline-block ;");
		out.print("   box-shadow: 1px 1px 1px #aaaaaa;");
		out.print("  margin-top: 10px; margin-bottom: 10px;  text-color:#ffffff;");
		out.print(" ");
		out.print("} ");
		out.print("    </style>");
		out.print("  <body>");
		out.print("<div class=\"card rcorners2\" style=\"width:40%; margin-top: 10%; margin-left: 30%\">");
		out.print("  <div class=\"row justify-content-center align-items-center\">");
		out.print(
				"  <form class=\"px-4 py-3 rcorners1\" style=\"width:90%; \"  method=\"post\" action=\"http://localhost:8080/ECommerceTask/SIgnIn\" >");

		UserDao userDao;
		try {

			userDao = UserDao.getInstance();
			Optional<User> userlogged = null;

			if (request.getParameter("email") != null) {
				email = request.getParameter("email");
				password = request.getParameter("password");
				userlogged = userDao.signIn(email, password);

				if (userlogged.isPresent()) {
					User userLoggedObject = userlogged.get();
					userID = userLoggedObject.getId();
					if (userLoggedObject.isVerfied()) {

						session.setAttribute(userIDKey, userID);
						session.setAttribute(userVerifykey, 1);
						response.sendRedirect("/ECommerceTask//USER/Main.jsp");
					} else {

						session.setAttribute(userIDKey, userID);
						response.sendRedirect("/ECommerceTask/verify.jsp");
					}
				} else {
					out.print("    <span class=\"error\">");
					out.print("     Email OR Password Invalid");
					out.print("    </span>");

				}
			}

			out.print("    <div class=\"form-group\">");
			out.print("      <label for=\"exampleDropdownFormEmail1\">Email address</label>");
			out.print(
					"      <input type=\"email\" class=\"form-control\" id=\"exampleDropdownFormEmail1\" placeholder=\"email@example.com\" name=\"email\">");
			out.print("    </div>");
			out.print("    <div class=\"form-group\">");
			out.print("      <label for=\"exampleDropdownFormPassword1\">Password</label>");
			out.print(
					"      <input type=\"password\" class=\"form-control\" id=\"exampleDropdownFormPassword1\" placeholder=\"Password\" name=\"password\">");
			out.print("    </div>");
			out.print("    ");
			out.print("    <div class=\"form-group\">");
			out.print("      <div class=\"form-check\">");
			out.print("       ");
			out.print("      </div>");
			out.print("    </div>");
			out.print("   ");
			out.print(
					"	 <a    href=\"http://localhost:8080/ECommerceTask/ForgetPassword.jsp\"> <p \"style=\"margin-left: 5%\"   >Forget Your Passsword?</p></a>");
			
			out.print("	  <button type=\"submit\" style=\"width:90%; margin-left: 5%\"   class=\"btn btn-primary rcorners2\" >Sign in</button><br><br>");
			out.print(
					"	 <a    href=\"http://localhost:8080/ECommerceTask/SignUp.html\"> <button type=\"button\"  class=\"btn btn-primary rcorners2\"style=\"width:90%;background: #C0C0C0;margin-left: 5%\"   >Sign up</button></a>");
			
			out.print("  </form>");
			out.print("  ");
			out.print("    <div class=\"dropdown-divider\"></div>");
			out.print("");
			out.print("  </div>");
			out.print("  </div>");
			out.print("    <!-- Optional JavaScript -->");
			out.print("    <!-- jQuery first, then Popper.js, then Bootstrap JS -->");
			out.print(
					"    <script src=\"https://code.jquery.com/jquery-3.5.1.slim.min.js\" integrity=\"sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj\" crossorigin=\"anonymous\"></script>");
			out.print(
					"    <script src=\"https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js\" integrity=\"sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo\" crossorigin=\"anonymous\"></script>");
			out.print(
					"    <script src=\"https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js\" integrity=\"sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI\" crossorigin=\"anonymous\"></script>");
			out.print("  </body>");
			out.print("</html>");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
