package UserAuth;

import java.util.Random;

public class OTPGenareate {

	  
	    // This our Password generating method 
	    // We have use static here, so that we not to 
	    // make any object for it 
	    static String geek_Password(int len) 
	    { 
	  
	        // A strong password has Cap_chars,  
	        // numeric value So we are using all of 
	        // them to generate our password 
	        String Capital_chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"; 
	        String numbers = "0123456789"; 
	        
	  
	  
	        String values = Capital_chars +numbers ; 
	  
	        // Using random method 
	        Random rndm_method = new Random(); 
	  
	        char[] password = new char[len]; 
	  
	        for (int i = 0; i < len; i++) 
	        { 
	            // Use of charAt() method : to get character value 
	            // Use of nextInt() as it is scanning the value as int 
	            password[i] = 
	              values.charAt(rndm_method.nextInt(values.length())); 
	  
	        } 
	        return new String(password); 
	    } 
	} 

