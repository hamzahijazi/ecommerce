package UserAuth;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.jdbi.v3.core.statement.UnableToExecuteStatementException;


/**
 * Servlet implementation class SignUp
 */
@WebServlet("/SignUp")
public class SignUp extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public SignUp() {

		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
		PrintWriter out = response.getWriter();
		String Firstname = request.getParameter("Firstname");
		String lastname = request.getParameter("lastname");
		String Username = request.getParameter("Username");
		String email = request.getParameter("email");
		String password = request.getParameter("password");
		String verifycCode=OTPGenareate.geek_Password(5);
		HttpSession session = request.getSession(true);
		try {
		User user = new User(Firstname, lastname, Username, email, "1", password,verifycCode);
		UserDao userDao;
		userDao = UserDao.getInstance();
		User createdUser=userDao.signUp(user);
		String userIDKey = "userID";
		String userID = "ABCD";
		userID = createdUser.getId();
		session.setAttribute(userIDKey, userID);
		String messageBody ="<h3>this is your Verification code</h3>\n\n"
				+ "<h1> H-"+verifycCode+"</h1>" ;
		RequestDispatcher reqdispatcherToEmail= request.getRequestDispatcher("/SendEmail");  
		request.setAttribute("emailto", email);
		request.setAttribute("messagebody",messageBody);
		reqdispatcherToEmail.include(request, response);
		response.sendRedirect("/ECommerceTask/verify.jsp");
		
		
		
		
		}

		catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch(UnableToExecuteStatementException e) {
			out.print("    <style>");
			out.print("   .error {");
			out.print("  background:#e8505b ;");
			out.print("   width: 15% ; padding: 10px ;");
			out.print("   border-radius:15px 50px 30px; ;");
			out.print("  position: relative; ");
			out.print("   display: inline-block ;");
			out.print("   box-shadow: 11px 11px 11px #aaaaaa;");
			out.print("  margin-top: 10px; margin-bottom: 10px; margin-left: 43%; text-color:#ffffff;");
			out.print(" ");
			out.print("} ");
			out.print("    </style>");
			out.print("    <span class=\"error\">");
			out.print("     User Name taken");
			out.print("    </span>");
			RequestDispatcher reqdispatcherToEmail= request.getRequestDispatcher("/SignUp.html");  
			reqdispatcherToEmail.include(request, response);
		}
		
	}

}
