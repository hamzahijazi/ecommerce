package UserAuth;

import java.sql.Timestamp;
import java.util.GregorianCalendar;

public class User {
	String firstname;
	String lastname;
	String username;
	String email;
	String id;
	String password;
	String verifycode;
	Timestamp  verifingdate;
	boolean verfied;
	public User() {
		super();
	}
	public User(String firstname, String lastname, String username, String email, String id, String password,
			String verifycode) {
		super();
		this.firstname = firstname;
		this.lastname = lastname;
		this.username = username;
		this.email = email;
		this.id = id;
		this.password = password;
		this.verifycode = verifycode;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getVerifycode() {
		return verifycode;
	}
	public void setVerifycode(String verifycode) {
		this.verifycode = verifycode;
	}
	public Timestamp getVerifingdate() {
		return verifingdate;
	}
	public void setVerifingdate(Timestamp verifingdate) {
		this.verifingdate = verifingdate;
	}
	public boolean isVerfied() {
		return verfied;
	}
	public void setVerfied(boolean verfied) {
		this.verfied = verfied;
	}
	@Override
	public String toString() {
		return "User [firstname=" + firstname + ", lastname=" + lastname + ", username=" + username + ", email=" + email
				+ ", id=" + id + ", password=" + password + ", verifycode=" + verifycode + ", verifingdate="
				+ verifingdate + ", verfied=" + verfied + "]";
	}
	
	



}
