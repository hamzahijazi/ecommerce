package UserAuth;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mysql.fabric.Response;

/**
 * Servlet implementation class ForgetPassword
 */
@WebServlet("/ForgetPassword")
public class ForgetPassword extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ForgetPassword() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		UserDao userDao;
		System.out.println("code hasbean ended");

		try {
			userDao = UserDao.getInstance();

			if (request.getParameter("newPassword") != null) {
				String password = request.getParameter("newPassword");
				String userid = request.getParameter("userid");
				String valid=request.getParameter("valid");
				if(valid.equals("valid"))
				{
					userDao.reSetPassword(userid, password);
					response.sendRedirect("http://localhost:8080/ECommerceTask/SIgnIn");
				}
				
			} 
			String email=null;
			if (request.getParameter("email") != null) {
			 email=request.getParameter("email") ;
			
			}
			else
			if (request.getAttribute("email") != null) {
				 email=(String) request.getAttribute("email") ;
			}
			if (email!= null) {

				out.println((String) (request.getParameter("email")));

				User user = userDao.getUserByEmail(email);
				out.println(user);
				if (user != null) {
					String Body = "<html><head>\r\n"
							+ "<META content=\"text/html; charset=us-ascii\" http-equiv=Content-Type></head><body>    <form method=\"post\" action='http://localhost:8080/ECommerceTask/NewPassword.jsp'>\r\n"
							+ " <input type='hidden' name='usercode' value=\"" + user.id + "\">    "
							+ " <input type='hidden' name='time' value=\"" + System.currentTimeMillis()/1000 + "\">    "
							+ "  <button type=\"submit\" class=\"btn btn-primary\">Submit</button></form></body></html>";
					request.setAttribute("messagebody", Body);
					request.setAttribute("emailto", user.email);

					RequestDispatcher reqDis = request.getRequestDispatcher("/SendEmail");
					reqDis.forward(request, response);
				}
			}
		} catch (ClassNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
