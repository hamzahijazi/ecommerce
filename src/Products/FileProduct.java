package Products;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class FileProduct implements Product {

	@Override
	public List<ProductData> getProduct() {
		List<ProductData> products = new ArrayList<ProductData>();

		try {
			InputStream Repository = new FileInputStream(
					"C:\\Users\\USER\\Covid-19Task\\ECommerceTask\\WebContent\\WEB-INF\\Product.csv");

			BufferedReader input = new BufferedReader(new InputStreamReader(Repository));

			String inputLine;
			String[] line = null;
			input.readLine();

			while ((inputLine = input.readLine()) != null) {

				line = inputLine.replace("\"", "").split(";");
				try {
					ProductData newProduct = new ProductData(line[0], line[1], line[2], line[3], line[4], line[5],
							line[6], line[7]);
					Double.valueOf(line[7]);
					products.add(newProduct);
				} catch (Exception e) {
				continue;
			
				}

			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return products;
	}

}
