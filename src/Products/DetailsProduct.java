package Products;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class DetailsProduct
 */
@WebServlet("/USER/DetailsProduct")
public class DetailsProduct extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public DetailsProduct() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		PrintWriter out = response.getWriter();
		System.out.print(request.getParameter("productcode"));
		if (request.getParameter("productcode") != null) {
			String code = (String) request.getParameter("productcode");
			ProductFactory productsObject = new ProductFactory();

			Product productRepository = productsObject.getProduct("File");
			List<ProductData> products = productRepository.getProduct();

			for (ProductData product : products) {

				if (product.productCode.equals(code.trim())) {
					RequestDispatcher reqdis= request.getRequestDispatcher("/USER/Details.jsp");
					request.setAttribute("product",product);
					reqdis.include(request, response);
				}

			}
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
