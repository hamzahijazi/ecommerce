package Products;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class AddToCart
 */
@WebServlet("/USER/AddToCart")
public class AddToCart extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AddToCart() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		String ProductlistKey = "Productlist";
		ArrayList<ProductData> Productlist;
		PrintWriter out = response.getWriter();
		HttpSession session = request.getSession(true);
		if (session.getAttribute(ProductlistKey) != null) {
			Productlist=(ArrayList<ProductData>) session.getAttribute(ProductlistKey);
		}
		else
		{
			Productlist=new ArrayList();
		}

		if (request.getParameter("productcode") != null) {
			String code = (String) request.getParameter("productcode");
			ProductFactory productsObject = new ProductFactory();

			Product productRepository = productsObject.getProduct("File");
			List<ProductData> products = productRepository.getProduct();

			for (ProductData product : products) {
				
				if (product.productCode.equalsIgnoreCase(code.trim())) {
					
					Productlist.add(product);
					
					
				}

			}
			session.setAttribute(ProductlistKey, Productlist);
		}
		response.sendRedirect("/ECommerceTask/USER/Main.jsp");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
