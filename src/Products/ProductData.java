package Products;

public class ProductData {
	String productCode;
	String productName;
	String productLine;
	String productScale;
	String productVendor;
	String productDescription;
	String quantityInStock;
	String buyPrice;

	@Override
	public String toString() {
		return "ProductData [productCode=" + productCode + ", productName=" + productName + ", productLine="
				+ productLine + ", productScale=" + productScale + ", productVendor=" + productVendor
				+ ", productDescription=" + productDescription + ", quantityInStock=" + quantityInStock + ", buyPrice="
				+ buyPrice + "]";
	}

	public ProductData() {
		super();
	}

	public ProductData(String productCode, String productName, String productLine, String productScale,
			String productVendor, String productDescription, String quantityInStock, String buyPrice) {
		super();
		this.productCode = productCode;
		this.productName = productName;
		this.productLine = productLine;
		this.productScale = productScale;
		this.productVendor = productVendor;
		this.productDescription = productDescription;
		this.quantityInStock = quantityInStock;
		this.buyPrice = buyPrice;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getProductLine() {
		return productLine;
	}

	public void setProductLine(String productLine) {
		this.productLine = productLine;
	}

	public String getProductScale() {
		return productScale;
	}

	public void setProductScale(String productScale) {
		this.productScale = productScale;
	}

	public String getProductVendor() {
		return productVendor;
	}

	public void setProductVendor(String productVendor) {
		this.productVendor = productVendor;
	}

	public String getProductDescription() {
		return productDescription;
	}

	public void setProductDescription(String productDescription) {
		this.productDescription = productDescription;
	}

	public String getQuantityInStock() {
		return quantityInStock;
	}

	public void setQuantityInStock(String quantityInStock) {
		this.quantityInStock = quantityInStock;
	}

	public String getBuyPrice() {
		return buyPrice;
	}

	public void setBuyPrice(String buyPrice) {
		this.buyPrice = buyPrice;
	}

}
