package Products;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.coyote.Request;

/**
 * Servlet implementation class Cart
 */
@WebServlet("/USER/Cart")
public class Cart extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Cart() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@SuppressWarnings("unchecked")
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		RequestDispatcher reqDis =request.getRequestDispatcher("/USER/Navbar.jsp");
		reqDis.include(request, response);
		HttpSession session = request.getSession(true);
		response.setContentType("text/html");
		double total = 0;
		String ProductlistKey = "Productlist";
		ArrayList<ProductData> Productlist;
		PrintWriter out = response.getWriter();
		System.out.print(request.getParameter(ProductlistKey));
		out.print("<!doctype html>");
		out.print("<html lang=\"en\">");
		out.print("");
		out.print("  <head>");
		out.print("    <!-- Required meta tags -->");
		out.print("    <meta charset=\"utf-8\">");
		out.print("    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\">");
		out.print("");
		out.print("    <!-- Bootstrap CSS -->");
		out.print(
				"    <link rel=\"stylesheet\" href=\"https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css\" integrity=\"sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk\" crossorigin=\"anonymous\">");
		out.print("");
		out.print("    <title>Hello, world!</title>");
		out.print("    ");
		out.print("  ");
		out.print("    </head>");
		out.print("    <style>");
		out.print("   .rcorners2 {");
		out.print("  border-radius: 15px 50px 30px;");
		out.print("  padding: 20px; ");
		out.print("  background: #4F6367;");
		out.print(" ");
		out.print("}.rcorners1 {\r\n" + 
				"	border-radius: 15px 50px 30px;\r\n" + 
				"	padding: 20px;\r\n" + 
				"	background: #C0C0C0;\r\n" + 
				"	text-color: #00C0C0;\r\n" + 
				"} ");
		out.print("   .error {");
		out.print("  background:#e8505b ;");
		out.print("   padding: 10px ;");
		out.print("   border-radius:15px 50px 30px; ;");
		out.print("  position: relative; ");
		out.print("   display: inline-block ;");
		out.print("   box-shadow: 1px 1px 1px #aaaaaa;");
		out.print("  margin-top: 10px; margin-bottom: 10px;  text-color:#ffffff;");
		out.print(" ");
		out.print("} ");
		out.print("    </style>");
		out.print("  <body>");

		if (session.getAttribute(ProductlistKey) != null) {
			Productlist = (ArrayList<ProductData>) session.getAttribute(ProductlistKey);
			for (ProductData product : Productlist) {

				out.println("<div class=\"rcorners2\"");
				out.println("			style=\"display: inline-block; margin-left: 5rem; margin-top: 6rem;\">");
				out.println("			<div class=\"card\"");
				out.println("				style=\"width: 20rem; border-radius: 15px 50px 30px;\">");
				out.println("				<div class=\"card-body\">");
				out.println(
						"				<img class=\"card-img-top\"style=\"width:95%; border-radius: 15px 50px 30px;\" src=\"https://www.cycleworld.com/resizer/s4jDG0jBLAFlfU9QcZbdXxYiaNI=/799x599/cloudfront-us-east-1.images.arcpublishing.com/bonnier/34E5ITNNQJEHNGS7PUQ3TEAU6Y.jpg\" alt=\"Card image\">");
				out.println("					<h5 class=\"card-title\">");

				out.println(product.getProductName());

				out.println("					</h5>");
				out.println("				</div>");
				out.println("				<div class=\"card-body\">");
				out.println("");
				out.println("");

				out.println("  <ul class=\"list-group list-group-flush\">");
				out.println(
						"    <li class=\"list-group-item\">" + "In Stock: " + product.getQuantityInStock() + "</li>");
				total += Double.valueOf(product.getBuyPrice());
				out.println("    <li class=\"list-group-item\">" + "Price: " + product.getBuyPrice() + "$</li>");

				out.println("  </ul>");

				out.println("");
				out.println("				</div>");
				out.println("			</div>");
				out.println("			</div>");

			}
			out.println("<div class=\"rcorners1\"");
			out.println("			style=\"display: inline-block; margin-left: 5rem; margin-top: 6rem;\">");
			out.println("			<div class=\"card\"");
			out.println("				style=\"width: 20rem; border-radius: 15px 50px 30px;\">");
			out.println("				<div class=\"card-body\">");
			out.println("<h2 class=\"card-title\">");
			out.println("Total is: " +String.format("%.2f", total)+"$");
			out.println("</h2>");
			out.println("			</div>");
			out.println("		</div>");
			out.println("		</div>");
		} else {

		}
		out.print("    <!-- Optional JavaScript -->");
		out.print("    <!-- jQuery first, then Popper.js, then Bootstrap JS -->");
		out.print(
				"    <script src=\"https://code.jquery.com/jquery-3.5.1.slim.min.js\" integrity=\"sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj\" crossorigin=\"anonymous\"></script>");
		out.print(
				"    <script src=\"https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js\" integrity=\"sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo\" crossorigin=\"anonymous\"></script>");
		out.print(
				"    <script src=\"https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js\" integrity=\"sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI\" crossorigin=\"anonymous\"></script>");
		out.print("  </body>");
		out.print("</html>");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
